import os

from django.template.loader import get_template
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
import base64
from pdf2image import convert_from_path
from rest_framework.viewsets import ReadOnlyModelViewSet
from weasyprint import CSS, HTML
from weasyprint.text.fonts import FontConfiguration

from apps.certificate.api.serializers import CertificateSerializer
from apps.certificate.models import UserCertificate
from apps.courses.models import Course


@api_view(['POST'])
@permission_classes([AllowAny])
def generate_certificate(request):
    username = request.data.get('user')
    first_name = request.data.get('first_name')
    last_name = request.data.get('last_name')
    user_id = request.data.get('id')
    course_slug = request.data.get('course_slug', None)
    director = request.data.get('director', None)
    course = Course.objects.filter(slug=course_slug).first()

    if not course:
        return Response({'status': 0, "msg": f"course not found with this slug: {course_slug}"})
    user_certificate = UserCertificate.objects.create(
        user_id=user_id,
        course=course,
    )

    template = get_template("certificate/certificate_template.html")
    context = {
        "serial_number": user_certificate.id,
        "username": username,
        "first_name": first_name,
        "last_name": last_name,
        "course_name": course.name,
        "data_created": user_certificate.created_at,
        "director": director,
        "img_string": image_file_path_to_base64_string('templates/certificate/certificate_img.jpg')

    }

    html = template.render(context)

    font_config = FontConfiguration()

    css = CSS(string='''
        @font-face {
          font-family: 'Hannari-Regular';
          src: local('Hannari-Regular')
        }
    ''', font_config=font_config)
    try:
        HTML(string=html).write_pdf(f'user_certificate/{username}_{course.name}.pdf', stylesheets=[css],
                                    font_config=font_config)
    except:
        os.mkdir("user_certificate/")
        HTML(string=html).write_pdf(f'user_certificate/{username}_{course.name}.pdf', stylesheets=[css],
                                    font_config=font_config)

    f_pdf = open(f'user_certificate/{username}_{course.name}.pdf', "rb")
    user_certificate.certificate_pdf.save(f'{username}_{course.name}.pdf', f_pdf)
    f_pdf.close()
    # PDF to Image

    images = convert_from_path(f'user_certificate/{username}_{course.name}.pdf')

    images[0].save(f'user_certificate/{username}_{course.name}.jpg', 'JPEG')
    f = open(f'user_certificate/{username}_{course.name}.jpg', "rb")
    user_certificate.certificate_image.save(f'{username}_{course.name}.jpg', f)

    f.close()

    if os.path.exists(f'user_certificate/{username}_{course.name}.jpg'):
        os.remove(f'user_certificate/{username}_{course.name}.jpg')
    else:
        print("The file does not exist")
    if os.path.exists(f'user_certificate/{username}_{course.name}.pdf'):
        os.remove(f'user_certificate/{username}_{course.name}.pdf')
    else:
        print("The file does not exist")
    return Response({'status': 1,
                     "data": CertificateSerializer(user_certificate).data})


def generate_certificate_for_bot(user, course, score, max_score):
    user_certificate = UserCertificate.objects.create(
        user=user,
        course=course,
    )
    director = 'Malikov Mardon'

    template = get_template("certificate/certificate_template.html")
    context = {
        "serial_number": user_certificate.id,
        "username": user.user_name,
        "first_name": user.name,
        "last_name": user.last_name,
        "course_name": course.name,
        "data_created": user_certificate.created_at,
        "director": director,
        "score": score/max_score * 100,
        "img_string": image_file_path_to_base64_string('templates/certificate/certificate_img.jpg')

    }

    html = template.render(context)

    font_config = FontConfiguration()

    css = CSS(string='''
        @font-face {
          font-family: 'Hannari-Regular';
          src: local('Hannari-Regular')
        }
    ''', font_config=font_config)
    try:
        HTML(string=html).write_pdf(f'user_certificate/{user.chat_id}_{course.name}.pdf', stylesheets=[css],
                                    font_config=font_config)
    except:
        os.mkdir("user_certificate/")
        HTML(string=html).write_pdf(f'user_certificate/{user.chat_id}_{course.name}.pdf', stylesheets=[css],
                                    font_config=font_config)

    f_pdf = open(f'user_certificate/{user.chat_id}_{course.name}.pdf', "rb")
    user_certificate.certificate_pdf.save(f'{user.chat_id}_{course.name}.pdf', f_pdf)
    f_pdf.close()
    # PDF to Image

    images = convert_from_path(f'user_certificate/{user.chat_id}_{course.name}.pdf')

    images[0].save(f'user_certificate/{user.chat_id}_{course.name}.jpg', 'JPEG')
    f = open(f'user_certificate/{user.chat_id}_{course.name}.jpg', "rb")
    user_certificate.certificate_image.save(f'{user.chat_id}_{course.name}.jpg', f)
    user_certificate.save()
    f.close()

    if os.path.exists(f'user_certificate/{user.chat_id}_{course.name}.jpg'):
        os.remove(f'user_certificate/{user.chat_id}_{course.name}.jpg')
    else:
        print("The file does not exist")
    if os.path.exists(f'user_certificate/{user.chat_id}_{course.name}.pdf'):
        os.remove(f'user_certificate/{user.chat_id}_{course.name}.pdf')
    else:
        print("The file does not exist")
    return user_certificate


def image_file_path_to_base64_string(filepath):
    with open(filepath, 'rb') as f:
        return base64.b64encode(f.read()).decode()


class UserCertificateList(ReadOnlyModelViewSet):
    serializer_class = CertificateSerializer
    permission_classes = [AllowAny]

    def get_queryset(self):
        user_id = self.request.query_params.get('user_id')
        certificate = UserCertificate.objects.filter(user_id=user_id).order_by('-created_at')
        return certificate

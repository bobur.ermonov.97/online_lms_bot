from rest_framework import serializers

from apps.certificate.models import UserCertificate
from apps.courses.models import Course
from config.settings import MEDIA_URL, APP_URL


class CourseCertificateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Course
        fields = ['id', 'name', 'description', 'slug']


class CertificateSerializer(serializers.ModelSerializer):
    certificate_image = serializers.SerializerMethodField()
    certificate_pdf = serializers.SerializerMethodField()
    course = serializers.SerializerMethodField()

    class Meta:
        model = UserCertificate
        fields = '__all__'

    def get_certificate_pdf(self, obj):
        return f"{APP_URL}{MEDIA_URL}{obj.certificate_pdf}"

    def get_certificate_image(self, obj):
        return f"{APP_URL}{MEDIA_URL}{obj.certificate_image}"

    def get_course(self, obj):
        return CourseCertificateSerializer(obj.course).data

from adminsortable2.admin import SortableAdminMixin
from django.contrib import admin

from apps.certificate.models import UserCertificate


# class LessonInline(admin.StackedInline):
#     model = Lesson
#     extra = 0


class UserCertificateAdmin(admin.ModelAdmin):
    class Meta:
        model = UserCertificate


class UserCertificateSortAdmin(UserCertificateAdmin):
    # a list of displayed columns name.
    # search_fields = ('name',)
    list_display = ['id', "user", "course", "created_at", "updated_at", ]
    list_filter = ("user", "course", "created_at", "updated_at",)
    # list_editable = ("status",)
    # inlines = [LessonInline, ]
    list_display_links = ['user', ]


admin.site.register(UserCertificate, UserCertificateSortAdmin)


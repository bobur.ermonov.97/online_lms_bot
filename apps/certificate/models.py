from django.db import models

from apps.bot_manager.models import TelegramUsers
from apps.courses.models import Course


# Create your models here.

def get_user_certificate_img(instance, filename):
    return "user_certificate/images/%s" % (filename)


def get_user_certificate_pdf(instance, filename):
    return "user_certificate/pdf/%s" % (filename)


class UserCertificate(models.Model):
    user = models.ForeignKey(TelegramUsers, on_delete=models.CASCADE, related_name='certificates')
    course = models.ForeignKey(Course, on_delete=models.SET_NULL, null=True, blank=True)
    certificate_image = models.ImageField(upload_to=get_user_certificate_img, blank=True, null=True)
    certificate_pdf = models.FileField(upload_to=get_user_certificate_pdf, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return f'{self.user} - {self.course}'

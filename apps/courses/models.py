import datetime
from django.db import models

# Create your models here.
from apps.accounts.models import User
from apps.bot_manager.models import TelegramUsers
from config.utils import generate_unique_slug


def get_category(instance, filename):
    return "categories/%s" % (filename)


def get_course(instance, filename):
    return "courses/%s" % (filename)


def get_lesson(instance, filename):
    return "lessons/%s" % (filename)


class Category(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    image = models.ImageField(upload_to='category', default='category/default.png')
    slug = models.SlugField(blank=True, null=True, unique=True, max_length=250)
    order = models.IntegerField(default=0)
    status = models.BooleanField(default=False)
    description = models.CharField(max_length=1000, blank=False, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('order',)

    def save(self, *args, **kwargs):
        if not self.slug:
            if len(self.name) > 0:
                self.slug = generate_unique_slug(self, 'name')
        super(Category, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return "/categories/%s/" % self.slug


class Course(models.Model):
    TYPE_CHOICES = (
        ('easy', "Easy"),
        ('medium', "Medium"),
        ('difficult', "Difficult"),
    )
    name = models.CharField(max_length=255, blank=False, null=True)
    description = models.TextField(max_length=1000, blank=False, null=True)
    image = models.ImageField(upload_to=get_course, default='courses/default.png')
    # video = models.FileField(upload_to=get_course, blank=True, null=True)
    video_url = models.URLField(blank=True, null=True)
    slug = models.SlugField(blank=True, null=True, unique=True, max_length=250)
    author = models.ForeignKey(User, related_name='author_courses', on_delete=models.SET_NULL, null=True, blank=True)
    status = models.BooleanField(default=False)
    order = models.IntegerField(default=0)
    # price = models.IntegerField(default=0)
    difficulty = models.CharField(choices=TYPE_CHOICES, max_length=255, default='easy')
    category = models.ForeignKey(Category, related_name='category', on_delete=models.SET_NULL, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.name)

    def save(self, *args, **kwargs):
        if not self.slug:
            if len(self.name) > 0:
                self.slug = generate_unique_slug(self, 'name')
        super(Course, self).save(*args, **kwargs)

    class Meta:
        ordering = ('order',)

    def hasPermission(self, user_id):
        return self.course_buy.filter(user_id=user_id, end_date__gt=datetime.datetime.now()).exists()

    def get_absolute_url(self):
        return "/categories/%s/%s/" % (self.slug, self.slug)


class Section(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    description = models.CharField(max_length=1000, blank=False, null=True)
    course = models.ForeignKey(Course, related_name='sections', on_delete=models.SET_NULL, null=True, blank=True)
    slug = models.SlugField(blank=True, null=True, unique=True, max_length=250)
    status = models.BooleanField(default=True)
    order = models.PositiveIntegerField(default=0, blank=False, null=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.name)

    def save(self, *args, **kwargs):
        if not self.slug:
            if len(self.name) > 0:
                self.slug = generate_unique_slug(self, 'name')
        super(Section, self).save(*args, **kwargs)

    class Meta(object):
        ordering = ['order']


class Lesson(models.Model):
    name = models.CharField(max_length=255, blank=False, null=True)
    description = models.CharField(max_length=1000, blank=True, null=True, default='')
    # content = models.TextField(blank=True, null=True, default='')
    file = models.FileField(upload_to=get_lesson, blank=True, null=True)
    # video = models.FileField(upload_to='lesson/video', blank=True, null=True)
    video_url = models.URLField(blank=True, null=True)
    course = models.ForeignKey(Course, blank=True, null=True, related_name='lessons', on_delete=models.SET_NULL)
    section = models.ForeignKey(Section, blank=True, null=True, related_name='lessons', on_delete=models.SET_NULL)
    status = models.BooleanField(default=True)
    # is_demo = models.BooleanField(default=False)
    order = models.IntegerField(default=0)
    # duration = models.IntegerField(default=0)
    # width = models.IntegerField(default=0)
    # height = models.IntegerField(default=0)
    slug = models.SlugField(blank=True, null=True, unique=True, max_length=250)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.name)

    class Meta(object):
        ordering = ['order']

    def save(self, *args, **kwargs):
        if not self.slug:
            if len(self.name) > 0:
                self.slug = generate_unique_slug(self, 'name')
        super(Lesson, self).save(*args, **kwargs)

    def hasPermission(self, user_id):
        # if self.is_demo == 1:
        #     return True
        # return self.section.course.course_buy.filter(user_id=user_id, end_date__gt=datetime.datetime.now()).exists()
        return True


class Review(models.Model):
    comment = models.CharField(max_length=255, blank=False, null=True)
    rating = models.IntegerField(default=5)
    user = models.ForeignKey(TelegramUsers, related_name='reviews', on_delete=models.SET_NULL, blank=True, null=True)
    lesson = models.ForeignKey(Lesson, related_name='reviews', null=True, blank=True, on_delete=models.SET_NULL)
    course = models.ForeignKey(Course, related_name='reviews', null=True, blank=True, on_delete=models.SET_NULL)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.comment)


class CoursePassing(models.Model):
    user = models.ForeignKey(TelegramUsers, on_delete=models.SET_NULL, blank=True, null=True,
                             related_name='course_passing')
    course = models.ForeignKey(Course, related_name='course_passing', blank=False, null=True,
                               on_delete=models.SET_NULL)
    score = models.IntegerField(default=0)
    max_score = models.IntegerField(default=0)
    is_complate = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


class Question(models.Model):
    lesson = models.ForeignKey(Lesson, related_name='question', blank=True, null=True, on_delete=models.SET_NULL)
    question = models.CharField(max_length=2048, blank=False, null=True)
    details = models.TextField(blank=False, null=True)
    try_count = models.IntegerField(default=0)
    score = models.IntegerField(default=0)
    order = models.IntegerField(default=0)
    status = models.BooleanField(default=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.question)

    class Meta(object):
        ordering = ['order']


class Answer(models.Model):
    question = models.ForeignKey(Question, related_name='answer', blank=True, null=True, on_delete=models.SET_NULL)
    answer = models.CharField(max_length=2048, blank=False, null=True)
    details = models.CharField(max_length=2048, blank=False, null=True)
    score = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.answer)


class LessonPassing(models.Model):
    user = models.ForeignKey(TelegramUsers, on_delete=models.SET_NULL, blank=True, null=True,
                             related_name='lesson_passing')
    lesson = models.ForeignKey(Lesson, related_name='lesson_passing', blank=True, null=True,
                               on_delete=models.SET_NULL)
    score = models.IntegerField(default=0)
    max_score = models.IntegerField(default=0)
    question_ids = models.CharField(max_length=2048, blank=False, null=True)
    details = models.TextField(max_length=2048, blank=False, null=True)
    is_completed = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.lesson)


class QuestionPassing(models.Model):
    question = models.ForeignKey(Question, related_name='question_passing', blank=True, null=True,
                                 on_delete=models.SET_NULL)
    user = models.ForeignKey(TelegramUsers, on_delete=models.SET_NULL, blank=True, null=True,
                             related_name='question_passing')
    answer = models.ForeignKey(Answer, on_delete=models.SET_NULL, blank=False, null=True,
                               related_name='question_passing')
    details = models.CharField(max_length=2048, blank=False, null=True)
    score = models.IntegerField(default=0)
    try_count = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return f"{self.id}.{self.answer}"


class UserCourse(models.Model):
    user = models.ForeignKey(TelegramUsers, related_name='user_buy', on_delete=models.SET_NULL, blank=True, null=True)
    course = models.ForeignKey(Course, related_name='course_buy', on_delete=models.SET_NULL, null=True, blank=True)
    duration = models.IntegerField(default=0)
    description = models.CharField(max_length=1000, blank=True, null=True, default='')
    start_date = models.DateTimeField(null=True)
    end_date = models.DateTimeField(null=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return f"{self.course}"

    def is_buy(self, obj):
        if self.context['request'].user.is_anonymous:
            return 0
        buy = UserCourse.objects.filter(course=obj, user=self.context['request'].user,
                                        end_date__gt=datetime.datetime.now()).count()
        if buy >= 1:
            return 1
        else:
            return 0

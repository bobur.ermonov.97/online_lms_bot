from adminsortable2.admin import SortableAdminMixin
from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.utils.html import format_html

# Register your models here.
from .models import *


class CourseInline(admin.StackedInline):
    model = Course
    extra = 0


class CategoryAdmin(admin.ModelAdmin):
    class Meta:
        model = Category


class CategorySortAdmin(SortableAdminMixin, CategoryAdmin):
    # a list of displayed columns name.
    def image_tag(self, obj):
        return format_html('<img src="{}" style="height:50px;"/>'.format(obj.image.url))

    def image_tag_inline(self, obj):
        return format_html('<img src="{}" style="height:250px;"/>'.format(obj.image.url))

    image_tag.short_description = 'Image'
    image_tag_inline.short_description = 'Image Inline'

    readonly_fields = ('image_tag', 'image_tag_inline')
    search_fields = ('name',)
    list_display = ['order', 'id', 'image_tag', "name", "status", "created_at", "updated_at", ]
    list_filter = ("status", "created_at", "updated_at",)
    list_editable = ("status",)
    inlines = [CourseInline, ]
    list_display_links = ['name', 'image_tag']
    fieldsets = (
        (None, {
            'fields': (
                "name", "status", "image", "image_tag_inline",)
        }),

    )


admin.site.register(Category, CategorySortAdmin)


class SectionInline(admin.StackedInline):
    model = Section
    extra = 0


class CoursePassingInline(admin.StackedInline):
    model = CoursePassing
    extra = 0


class CourseAdmin(admin.ModelAdmin):
    class Meta:
        model = Course


class CourseSortAdmin(SortableAdminMixin, CourseAdmin):
    # a list of displayed columns name.
    def image_tag(self, obj):
        return format_html('<img src="{}" style="height:50px;"/>'.format(obj.image.url))

    def image_tag_inline(self, obj):
        return format_html('<img src="{}" style="height:250px;"/>'.format(obj.image.url))

    image_tag.short_description = 'Image'
    image_tag_inline.short_description = 'Image Inline'

    readonly_fields = ('image_tag', 'image_tag_inline')
    search_fields = ('name',)
    list_display = ['order', 'id', 'image_tag', "name", "status", "author", "category", "video_url", "created_at",
                    "updated_at", ]
    list_filter = ("status", "author", "category", "created_at", "updated_at",)
    list_editable = ("status",)
    inlines = [SectionInline, CoursePassingInline]
    list_display_links = ['name', 'image_tag']


admin.site.register(Course, CourseSortAdmin)


class LessonInline(admin.StackedInline):
    model = Lesson
    extra = 0


class SectionAdmin(admin.ModelAdmin):
    class Meta:
        model = Section


class SectionSortAdmin(SortableAdminMixin, SectionAdmin):
    # a list of displayed columns name.
    search_fields = ('name',)
    list_display = ['order', 'id', "name", "status", "course", "created_at", "updated_at", ]
    list_filter = ("status", "course", "created_at", "updated_at",)
    list_editable = ("status",)
    inlines = [LessonInline, ]
    list_display_links = ['name', ]


admin.site.register(Section, SectionSortAdmin)


class QuestionInline(admin.StackedInline):
    model = Question
    extra = 0


class LessonPassingInline(admin.StackedInline):
    model = LessonPassing
    extra = 0


class LessonAdmin(admin.ModelAdmin):
    class Meta:
        model = Lesson


class LessonSortAdmin(SortableAdminMixin, SectionAdmin):
    # a list of displayed columns name.
    search_fields = ('name',)
    list_display = ['order', 'id', "name", "status", "course", "section", "video_url", "created_at", "updated_at", ]
    list_filter = ("status", "course", "section", "created_at", "updated_at",)
    list_editable = ("status",)
    inlines = [QuestionInline, LessonPassingInline]
    list_display_links = ['name', ]


admin.site.register(Lesson, LessonSortAdmin)


class QuestionPassingInline(admin.StackedInline):
    model = QuestionPassing
    extra = 0


class AnswerInline(admin.StackedInline):
    model = Answer
    extra = 0


from django import forms


class QuestionAdminForm(forms.ModelForm):
    question = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = Question
        fields = "__all__"


class QuestionAdmin(admin.ModelAdmin):
    class Meta:
        model = Question


class QuestionSortAdmin(SortableAdminMixin, QuestionAdmin):
    # a list of displayed columns name.
    search_fields = ('question',)
    list_display = ['order', 'id', "question", "status", "lesson", "created_at", "updated_at", ]
    list_filter = ("status", "lesson", "created_at", "updated_at",)
    list_editable = ("status",)
    inlines = [AnswerInline, QuestionPassingInline]
    list_display_links = ['question', ]
    form = QuestionAdminForm

admin.site.register(Question, QuestionSortAdmin)


class AnswerAdmin(admin.ModelAdmin):
    class Meta:
        model = Answer


class AnswerSortAdmin(AnswerAdmin):
    # a list of displayed columns name.
    search_fields = ('answer',)
    list_display = ['id', "question", "answer", "score", "created_at", "updated_at", ]
    list_filter = ("question", "score", "created_at", "updated_at",)
    list_editable = ("score",)
    list_display_links = ['question', ]


admin.site.register(Answer, AnswerSortAdmin)


class LessonPassingSortAdmin(AnswerAdmin):
    # a list of displayed columns name.
    # search_fields = ('answer',)
    list_display = ['id', "user", "lesson", "score", "max_score", "is_completed", "created_at", "updated_at", ]
    list_filter = ("is_completed", "user", "lesson", "created_at", "updated_at",)
    list_editable = ("score", "is_completed")
    list_display_links = ['user', ]

    class Meta:
        model = LessonPassing


admin.site.register(LessonPassing, LessonPassingSortAdmin)


class QuestionPassingSortAdmin(AnswerAdmin):
    # a list of displayed columns name.
    # search_fields = ('answer',)
    list_display = ['id', "user", "question", "answer", "score", "created_at", "updated_at", ]
    list_filter = ("answer", "user", "question", "score", "created_at", "updated_at",)
    list_editable = ("score",)
    list_display_links = ['user', ]

    class Meta:
        model = QuestionPassing


admin.site.register(QuestionPassing, QuestionPassingSortAdmin)


class CoursePassingSortAdmin(AnswerAdmin):
    # a list of displayed columns name.
    # search_fields = ('answer',)
    list_display = ['id', "user", "course", "score", "max_score", "is_complate", "created_at", "updated_at", ]
    list_filter = ("course", "user", "score", "max_score", "is_complate", "created_at", "updated_at",)
    list_editable = ("score", "max_score", "is_complate",)
    list_display_links = ['user', ]

    class Meta:
        model = CoursePassing


admin.site.register(CoursePassing, CoursePassingSortAdmin)


class UserCourseSortAdmin(AnswerAdmin):
    # a list of displayed columns name.
    # search_fields = ('answer',)
    list_display = ['id', "user", "course", "duration", "start_date", "end_date", "created_at", "updated_at", ]
    list_filter = ("course", "user", "start_date", "end_date", "created_at", "updated_at",)
    list_editable = ("start_date", "end_date",)
    list_display_links = ['user', ]

    class Meta:
        model = UserCourse


admin.site.register(UserCourse, UserCourseSortAdmin)

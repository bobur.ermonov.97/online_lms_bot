from django.apps import AppConfig


class TelegramBotManagerConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.bot_manager'

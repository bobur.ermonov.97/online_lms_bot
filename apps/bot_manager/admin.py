
from django.contrib import admin

from apps.bot_manager.models import TelegramUsers
from apps.certificate.models import UserCertificate
from apps.courses.admin import CoursePassingInline, LessonPassingInline
from apps.courses.models import UserCourse


class UserCourseInline(admin.StackedInline):
    model = UserCourse
    extra = 0

class UserCertificateInline(admin.StackedInline):
    model = UserCertificate
    extra = 0


class TelegramUsersAdmin(admin.ModelAdmin):
    class Meta:
        model = TelegramUsers


class TelegramUsersSortAdmin(TelegramUsersAdmin):
    # a list of displayed columns name.
    search_fields = ('name',)
    list_display = ['id', 'chat_id', "name", "user_name", "last_name", "status", "register_status", "phone", ]
    list_filter = ("status", "register_status",)
    list_editable = ("status", "register_status",)
    inlines = [CoursePassingInline, UserCourseInline, UserCertificateInline]
    list_display_links = ['chat_id', ]


admin.site.register(TelegramUsers, TelegramUsersSortAdmin)


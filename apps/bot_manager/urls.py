from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from apps.bot_manager.views import BotUpdate, test

urlpatterns = [
    path('bot_manager/', csrf_exempt(BotUpdate.as_view())),
    path('test/', test),
]
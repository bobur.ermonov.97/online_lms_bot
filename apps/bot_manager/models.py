from django.db import models


class TelegramUsers(models.Model):
    name = models.CharField(max_length=50, null=True, blank=True)
    last_name = models.CharField(max_length=50, null=True, blank=True)
    user_name = models.CharField(max_length=50, null=True, blank=True)
    chat_id = models.CharField(max_length=50)
    status = models.BooleanField(default=False)
    register_status = models.BooleanField(default=False)
    phone = models.CharField(max_length=50, null=True, blank=True)
    language = models.CharField(max_length=10, null=True)
    password = models.CharField(max_length=10, default='12345678')
    block_bot = models.BooleanField(default=True, null=True, blank=True)

    def __str__(self):
        return str(self.name)

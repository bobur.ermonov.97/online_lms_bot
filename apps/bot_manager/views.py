import datetime
import json

from django.db.models import Max, Sum
from rest_framework.decorators import api_view
from telebot import TeleBot, types
from rest_framework.response import Response
from rest_framework.views import APIView
import sys

from apps.bot_manager.models import TelegramUsers
from apps.certificate.api.views import generate_certificate_for_bot
from apps.certificate.models import UserCertificate
from apps.courses.models import Category, Course, Section, Lesson, Question, QuestionPassing, UserCourse, LessonPassing, \
    CoursePassing

TOKEN = "6102180846:AAHjUXrII4gxGbVh0IeE6yV4biO66CRfwE4"
# TOKEN = "5994529609:AAGo8OU914xXM9HT2tCY1fYuNOrjjK7rTk4"

bot = TeleBot(TOKEN)


class BotUpdate(APIView):
    def post(self, request, *args, **kwargs):
        # Сюда должны получать сообщения от телеграм и далее обрабатываться ботом
        t_data = json.loads(request.body)
        json_str = request.body.decode('UTF-8')
        update = types.Update.de_json(json_str)
        bot.process_new_updates([update])
        try:
            chat_id = t_data['my_chat_member']
            chat_id = chat_id['chat']
            chat_id = chat_id['id']
            user = TelegramUsers.objects.all().filter(chat_id=chat_id).first()
            user.block_bot = False
            user.save()

        except:
            pass
        return Response({'code': 200})


def button_generate(message, data, key):
    # telegramuser = TelegramUsers.objects.filter(chat_id=message.from_user.id).first()

    inline_key = types.InlineKeyboardMarkup()
    for item in data:
        button = types.InlineKeyboardButton(text=item.name,
                                            callback_data=f"{key}#{item.id}")
        inline_key.add(button)
    bot.send_message(message.from_user.id, text="Kerakli bo'limni tanlang!",
                     reply_markup=inline_key, parse_mode='HTML')


def certificate_button_generate(message, data, key):
    # telegramuser = TelegramUsers.objects.filter(chat_id=message.from_user.id).first()

    inline_key = types.InlineKeyboardMarkup()
    for item in data:
        button = types.InlineKeyboardButton(text=item.course.name,
                                            callback_data=f"{key}#{item.id}")
        inline_key.add(button)
    bot.send_message(message.from_user.id, text="Kerakli bo'limni tanlang!",
                     reply_markup=inline_key, parse_mode='HTML')


def question_button_generate(message, data, key, lesson_id=None):
    try:
        telegramuser = TelegramUsers.objects.filter(chat_id=message.from_user.id).first()

        if not data:
            questoins_passings = QuestionPassing.objects.filter(user=telegramuser,
                                                                question__lesson_id=lesson_id).values(
                'question_id').annotate(
                Max('id'))
            result = []
            res = ''
            user_score = 0
            for questoins_passing in questoins_passings:
                q = QuestionPassing.objects.filter(user=telegramuser,
                                                   question_id=questoins_passing['question_id']).order_by(
                    '-created_at').first()
                result.append(q)
                user_score += q.answer.score
                res += f"id -> {q.question_id}\n" \
                       f"question -> {q.question.question}\n" \
                       f"answer -> {q.answer.answer}\n" \
                       f"score -> {q.answer.score}\n" \
                       f"\n******\n"
            max_score = Question.objects.filter(lesson_id=lesson_id).count()
            is_complate = True if user_score / max_score * 100 > 56 else False
            l_passing = LessonPassing.objects.create(
                user=telegramuser,
                lesson_id=lesson_id,
                max_score=max_score,
                score=user_score,
                details=res,
                is_completed=is_complate
            )
            print(result)
            if is_complate:
                inline_key = types.InlineKeyboardMarkup()
                button = types.InlineKeyboardButton(text='Keyingi mavzu',
                                                    callback_data=f"next_lesson#{lesson_id}")
                inline_key.add(button)
                bot.send_message(message.from_user.id, text=res,
                                 reply_markup=inline_key)
            else:
                markup_inline = types.InlineKeyboardMarkup()
                courses = types.InlineKeyboardButton(text="Test takroran ishlash!", callback_data=f"tests#{lesson_id}")
                markup_inline.add(courses)
                bot.send_message(message.from_user.id, text=res, reply_markup=markup_inline)

        else:
            data = list(map(int, data.split(', ')))
            print(data)
            question = Question.objects.filter(id=data.pop(0)
                                               ).prefetch_related('answer').first()
            print(question)
            data = str(list(data)).replace('[', '').replace(']', '')
            inline_key = types.InlineKeyboardMarkup()
            i = 0
            buttun_text = 'Javoblar \n'
            for item in question.answer.all():
                i += 1
                button = types.InlineKeyboardButton(text=str(i),
                                                    callback_data=f"{key}#{question.id}#{item.id}#{item.score}#{data}#{lesson_id}")
                buttun_text += f'{i}) {item.answer}\n'
                inline_key.add(button)
            bot.send_message(message.from_user.id, text=f"{question.question}\n{buttun_text}",
                             reply_markup=inline_key)
    except Exception as e:
        trace_back = sys.exc_info()[2]
        line = trace_back.tb_lineno
        bot.send_message(message.from_user.id, text=str(e) + str(line))


@bot.message_handler(commands=['start'])
def start_message(message):
    telegram_user = TelegramUsers.objects.all().filter(chat_id=message.from_user.id).first()
    print(message)
    try:
        if not telegram_user:
            name = message.from_user.first_name
            last_name = message.from_user.last_name
            user_name = message.from_user.username
            chat_id = message.from_user.id
            language = message.from_user.language_code

            telegram_user = TelegramUsers.objects.create(
                name=name,
                last_name=last_name,
                user_name=user_name,
                chat_id=chat_id,
                language=language

            )
    except Exception as e:
        bot.send_message(message.chat.id, str(e), )

    markup_inline = types.InlineKeyboardMarkup()
    courses = types.InlineKeyboardButton(text="Kategoriya", callback_data="categories")
    profile = types.InlineKeyboardButton(text='Profile', callback_data="profile")
    markup_inline.add(courses, profile)
    bot.send_message(message.chat.id, "Kerakli bo'limni tanlang!", reply_markup=markup_inline, parse_mode="HTML")


@bot.callback_query_handler(func=lambda call: True)
def answer(call):
    telegram_user = TelegramUsers.objects.all().filter(chat_id=call.from_user.id).prefetch_related('user_buy').first()

    try:
        if call.data == 'categories':
            categories = Category.objects.filter(status=True)
            bot.delete_message(call.message.chat.id, call.message.message_id)

            button_generate(message=call, data=categories, key='category')

        elif call.data.split('#')[0] == "category":
            category = Category.objects.filter(id=call.data.split('#')[1]).prefetch_related('category').first()
            text = f"Kategoriya nomi: <b>{category.name}</b>\n" \
                   f"Kategoriya haqida: <i>{category.description}</i>"
            bot.delete_message(call.message.chat.id, call.message.message_id)

            bot.send_photo(call.message.chat.id, photo=category.image, caption=text, parse_mode='HTML')
            # data = Course.objects.filter(status=True, category_id=call.data.split('#')[1])
            button_generate(message=call, data=category.category.filter(status=True), key='course')

        elif call.data.split('#')[0] == "course":
            course = Course.objects.filter(id=call.data.split('#')[1]).prefetch_related('sections').prefetch_related(
                'course_buy').first()
            if not course.course_buy.filter(user=telegram_user).first():
                UserCourse.objects.create(
                    course=course,
                    user=telegram_user,
                    start_date=datetime.datetime.now()
                )
            # data = Section.objects.filter(status=True, course_id=call.data.split('#')[1])
            bot.delete_message(call.message.chat.id, call.message.message_id)
            text = f"Kurs nomi: <b>{course.name}</b>\n" \
                   f"Kurs haqida: {course.description}\n" \
                   f"Muallif: <i>{course.author}</i>"
            bot.send_photo(call.message.chat.id, photo=course.image, caption=text, parse_mode='HTML')

            button_generate(message=call, data=course.sections.filter(status=True), key='section')

        elif call.data.split('#')[0] == "section":
            data = Lesson.objects.filter(status=True, section_id=call.data.split('#')[1])
            bot.delete_message(call.message.chat.id, call.message.message_id)

            button_generate(message=call, data=data, key='lesson')

        elif call.data.split('#')[0] == "lesson":
            lesson = Lesson.objects.filter(id=call.data.split('#')[1]).first()
            text = f"Mavzu nomi: <b>{lesson.name}</b>\n" \
                   f"Mavzu haqida: <i>{lesson.description}</i>\n" \
                   f"Video link: {lesson.video_url}"
            bot.send_message(call.message.chat.id, text=text, parse_mode='HTML')
            bot.send_document(call.message.chat.id, document=lesson.file, caption='Kontent')
            markup_inline = types.InlineKeyboardMarkup()
            courses = types.InlineKeyboardButton(text="Test", callback_data=f"tests#{lesson.id}")
            markup_inline.add(courses)
            bot.send_message(call.message.chat.id, 'Test ishlash!', reply_markup=markup_inline)

        elif call.data.split('#')[0] == "tests":
            questions = Question.objects.filter(lesson_id=call.data.split('#')[1], status=True).values_list('id', flat=True)
            print(questions)
            questions_ids = str(list(questions)).replace('[', '').replace(']', '')
            print(questions_ids)
            bot.delete_message(call.message.chat.id, call.message.message_id)
            question_button_generate(message=call, data=questions_ids, key='question',
                                     lesson_id=call.data.split('#')[1])

        elif call.data.split('#')[0] == "question":
            q_passing = QuestionPassing.objects.create(question_id=call.data.split('#')[1],
                                                       answer_id=call.data.split('#')[2],
                                                       user=telegram_user,
                                                       score=call.data.split('#')[3])

            bot.delete_message(call.message.chat.id, call.message.message_id)
            question_button_generate(message=call, data=call.data.split('#')[4], key='question',
                                     lesson_id=call.data.split('#')[5])
        elif call.data == 'profile':
            bot.delete_message(call.message.chat.id, call.message.message_id)

            markup_inline = types.InlineKeyboardMarkup()
            my_courses = types.InlineKeyboardButton(text="Mening kurslarim", callback_data=f"my_courses")
            my_certificates = types.InlineKeyboardButton(text="Mening sertifikatlarim",
                                                         callback_data=f"my_certificates")
            # my_passing = types.InlineKeyboardButton(text="Mening natijalarim", callback_data=f"my_passing")
            markup_inline.add(my_courses)
            markup_inline.add(my_certificates)
            # markup_inline.add(my_passing)
            bot.send_message(call.message.chat.id, "Kerakli bo'limni tanlang", reply_markup=markup_inline)

        elif call.data == 'my_courses':

            user_courses = telegram_user.user_buy.values_list('course_id', flat=True)
            courses = Course.objects.filter(id__in=user_courses, status=True)
            bot.delete_message(call.message.chat.id, call.message.message_id)

            button_generate(message=call, data=courses, key='course')

        elif call.data == 'my_certificates':

            certificates = telegram_user.certificates.all()

            bot.delete_message(call.message.chat.id, call.message.message_id)

            certificate_button_generate(message=call, data=certificates, key='certificates')

        elif call.data.split('#')[0] == 'next_lesson':
            lesson_old = Lesson.objects.filter(id=call.data.split('#')[1]).select_related('course').first()
            lesson_ids = LessonPassing.objects.filter(lesson__course=lesson_old.course, is_completed=True).values_list(
                'lesson_id', flat=True)
            lesson = Lesson.objects.exclude(id__in=lesson_ids).filter(course=lesson_old.course).order_by('id').first()

            if lesson:
                bot.delete_message(call.message.chat.id, call.message.message_id)
                text = f"Mavzu nomi: <b>{lesson.name}</b>\n" \
                       f"Mavzu haqida: <i>{lesson.description}</i>\n" \
                       f"Video link: {lesson.video_url}"
                bot.send_message(call.message.chat.id, text=text, parse_mode='HTML')
                bot.send_document(call.message.chat.id, document=lesson.file, caption='Kontent')
                markup_inline = types.InlineKeyboardMarkup()
                tests = types.InlineKeyboardButton(text="Test", callback_data=f"tests#{lesson.id}")
                markup_inline.add(tests)
                bot.send_message(call.message.chat.id, 'Test ishlash!', reply_markup=markup_inline)

            else:
                markup_inline = types.InlineKeyboardMarkup()
                complate_course = types.InlineKeyboardButton(text="Kursni tugatish",
                                                             callback_data=f"complate_course#{lesson_old.course.id}")
                markup_inline.add(complate_course)
                bot.send_message(call.message.chat.id,
                                 'Tabriklaymiz! Quyidagi tugmani bosib Certifikatingizni olishingiz mumkin!',
                                 reply_markup=markup_inline)
        elif call.data.split('#')[0] == "complate_course":
            course = Course.objects.filter(id=call.data.split('#')[1]).first()
            score = LessonPassing.objects.filter(user_id=1, lesson__course_id=3).values('lesson_id').annotate(
                Max('score')).aggregate(Sum('score__max'))
            max_score = LessonPassing.objects.filter(user_id=1, lesson__course_id=3).values('lesson_id').annotate(
                Max('max_score')).aggregate(Sum('max_score__max'))
            print(score)
            course_passing = CoursePassing.objects.create(
                user=telegram_user,
                course=course,
                is_complate=True,
                score=score['score__max__sum'],
                max_score=max_score['max_score__max__sum']
            )

            certificate = generate_certificate_for_bot(telegram_user, course, score['score__max__sum'],
                                                       max_score['max_score__max__sum'])
            bot.send_document(call.message.chat.id, document=certificate.certificate_pdf, caption='PDF')
            bot.send_document(call.message.chat.id, document=certificate.certificate_image, caption='IMG')

        elif call.data.split('#')[0] == "certificates":
            certificate = UserCertificate.objects.filter(id=call.data.split('#')[1]).first()

            bot.send_document(call.message.chat.id, document=certificate.certificate_pdf, caption='PDF')
            bot.send_document(call.message.chat.id, document=certificate.certificate_image, caption='IMG')

    except Exception as e:
        trace_back = sys.exc_info()[2]
        line = trace_back.tb_lineno
        bot.send_message(call.message.chat.id, text=str(e) + str(line), parse_mode='HTML')


@bot.message_handler(content_types=['text'])
def parol(message):
    global text

    try:
        telegram_user = TelegramUsers.objects.all().filter(chat_id=message.chat.id).first()
        if telegram_user.register_status == False:

            # User написал /start в диалоге с ботом
            # slug_result = models.TelegramUsers.objects.all().filter(chat_d=slug_test).first()
            parol = message.text
            if parol == telegram_user.password:

                keyboard = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
                reg_button = types.KeyboardButton(text=f"📞 {messages(telegram_user, 3)}", request_contact=True)
                keyboard.add(reg_button)
                response = bot.send_message(message.chat.id,
                                            messages(telegram_user, 2),
                                            reply_markup=keyboard)

            else:
                text = messages(telegram_user, 5)
        else:
            text = messages(telegram_user, 8)
    except:

        text = "🇺🇿  Botdagi buyruqlar ro'yxati:\n"
        text += "/manager - ro'yxatdan o'tish!\n"
        text += "/settings - tilni o'zgartirish uchun buyruq !\n\n"
        text += "Такой команды не найдено\n"
        text += "🇷🇺: Список команд в боте:\n"
        text += "/manager - Зарегистрировано!\n"
        text += "/settings - команда для смены языка!"

    bot.send_message(message.chat.id, text=str(text), parse_mode='HTML')


@bot.message_handler(content_types=['contact'])
def contact_handler(message):
    telegram_user = TelegramUsers.objects.all().filter(chat_id=message.chat.id).first()
    if telegram_user.register_status == False:
        phone = message.contact.phone_number

        telegram_user.phone = phone
        telegram_user.register_status = True
        telegram_user.save()

        text = messages(telegram_user, 4)

    else:
        text = messages(telegram_user, 6)

    bot.send_message(message.chat.id, text=text, parse_mode='HTML')


def messages(telegram_user, code):
    message = [
        {
            "uz": "Assalomu alaykum botdan ro'yxatdan o'tish uchun /manager buyrug'ini bosing!",
            "ru": "<b> Здравствуйте, нажмите /manager, чтобы зарегистрироваться с помощью бота! </b> \n\n",
        },
        {
            "uz": "🔒 Maxfiy parolni kiriting 🔑",
            "ru": "🔒  Введите секретный пароль 🔑"
        },
        {
            "uz": "☎️Telefon raqamingizni yuboring",
            "ru": "Отправьте свой номер телефона"
        },
        {
            "uz": "☎️Telefon raqamni yuborish",
            "ru": "☎️Отправить номер телефона"
        },
        {
            "uz": "🎉🎉🎉 Tabriklaymiz siz ro'yxatdan o'tdingiz 🎉🎉🎉\n Statusni faollashtirish uchun adminga murojat qiling 👨🏻‍💻",
            "ru": "🎉🎉🎉 Поздравляю вы зарегистрировались 🎉🎉🎉\n Свяжитесь с администратором, чтобы активировать статус 🏻‍💻"
        },
        {
            "uz": "Parolingiz xato 🔏",
            "ru": "Ваш пароль неверный 🔏"
        },
        {
            "uz": "🔐 Siz ro'yxatdan o'tgansiz",
            "ru": "🔐 Вы зарегистрированы"
        },
        {
            "uz": "🇺🇿 Til almashtirildi",
            "ru": "🇷🇺 Язык изменен"
        },
        {
            "uz": "Bundan buyruq topilmadi",
            "ru": "Заказ не найден"
        },
        {
            "uz": "Siz ro'yxatdan o'tmagansiz",
            "ru": "Вы не зарегистрированы"
        },

    ]

    if message[code] and message[code][telegram_user.language]:
        return message[code][telegram_user.language]
    elif message[code]:
        return message[code]["uz"]
    return ""


# ==================================================================
# manegerga xabar jo'natish va orderga yozish
# ==================================================================

def post_message_to_telegram(user_application=None, products_ids=None, currency_code=None):
    pass
    # currency_code = CurrencyExchange.objects.filter(currency=currency_code).first()
    # global text_ru
    # print(products_ids)
    #
    # users = getUser()
    # try:
    #
    #     products_ids_list = []
    #     quantity = {}
    #     total_price = 0
    #
    #     if products_ids:
    #         line = '____________________________________________'
    #
    #         text = f"<b>🙎‍♂️ F.I.O:  {user_application.fullname}</b>\n"
    #         text_ru = f"<b>🙎‍♂️ Ф.И.О.:  {user_application.fullname}</b>\n"
    #
    #         text += line + '\n'
    #         text_ru += line + '\n'
    #
    #         text += f"<b>📞 Foydalanuvchi telefoni :  {user_application.phone}</b>\n"
    #         text_ru += f"<b>📞  Номер пользователя:  {user_application.phone}</b>\n"
    #
    #         text += line + '\n'
    #         text_ru += line + '\n'
    #
    #         text += f"<b>  Izoh:  {user_application.comment}</b>\n"
    #         text_ru += f"<b>  Комментарий:  {user_application.comment}</b>\n"
    #
    #         text += line + '\n'
    #         text_ru += line + '\n'
    #         for item in products_ids:
    #             id, count = item.split(':')
    #             id, count = int(id), int(count)
    #             products_ids_list.append(id)
    #             quantity[id] = count
    #
    #         products = Product.objects.filter(id__in=products_ids_list)
    #
    #         t = 1
    #
    #         for product in products:
    #             order_item_price = 0
    #             count = 0
    #
    #             try:
    #                 product_count = quantity[product.id]
    #                 order_item_price = product.price * product_count
    #                 order_item = OrderItem.objects.create(
    #                     product=product,
    #                     user_application=user_application,
    #                     product_count=product_count,
    #                     price=order_item_price
    #
    #                 )
    #                 # order_item.order = order
    #                 # order_item.save()
    #             except Exception as e:
    #                 product_count = 0
    #                 print(str(e))
    #                 pass
    #             total_price += order_item_price
    #
    #             if count == 4:
    #                 for user in users:
    #                     if user[1] == "uz":
    #                         bot.send_photo(user[0],
    #                                        photo='https://api.ama.uz/media/images/products/Screenshot_2022-10-03_at_17.55.11.png',
    #                                        caption=text, parse_mode='HTML')
    #
    #                     else:
    #                         bot.send_photo(user[0],
    #                                        photo='https://api.ama.uz/media/images/products/Screenshot_2022-10-03_at_17.55.11.png',
    #                                        caption=text_ru, parse_mode='HTML')
    #                 text = ''
    #                 text_ru = ''
    #
    #             if (count > 0) and ((count - 4) % 15 == 0) and (count != 4):
    #                 for user in users:
    #                     if user[1] == "uz":
    #                         bot.send_message(user[0], text=text, parse_mode='HTML')
    #
    #                     else:
    #                         bot.send_message(user[0], text=text_ru, parse_mode='HTML')
    #                 text = ''
    #                 text_ru = ''
    #
    #             text_ru += f"{t}. <b>Название товар:</b>  {product.name_ru}\n" \
    #                        f" <b>Количество:</b> {product_count} \n" \
    #                        f" <b>Цена:</b> {order_item_price} \n"
    #             text += f"{t}. <b>Product nomi:</b>  {product.name_uz}\n" \
    #                     f"  <b>Soni:</b> {product_count} ta\n" \
    #                     f" <b>Summa:</b> {order_item_price} \n"
    #             text += line + '\n'
    #             text_ru += line + '\n'
    #             count += 1
    #             t += 1
    #         # order.total = total_price
    #         # order.save()
    #
    #         user_application.total_price = total_price
    #         user_application.save()
    #         text += f" <b>Jami Summa:</b> {total_price} EUR\n"
    #         text_ru += f" <b>Итого:</b> {total_price} EUR\n"
    #         if currency_code:
    #             text += f" <b>Jami Summa:</b> {float(total_price * currency_code.value)} {currency_code.currency}\n"
    #             text_ru += f" <b>Итого:</b> {float(total_price * currency_code.value)} {currency_code.currency}\n"
    #     # get fixing
    #
    #     else:
    #         respon = {
    #             "status": 0,
    #             "msg": "Order not add",
    #             "order_id": "none"
    #         }
    #         return Response(respon)
    #     #  user telegram send sms
    #     if count > 4:
    #         for user in users:
    #             if user[1] == "uz":
    #                 bot.send_message(user[0], text=text, parse_mode='HTML')
    #
    #             else:
    #                 bot.send_message(user[0], text=text_ru, parse_mode='HTML')
    #
    #     if count < 4:
    #         for user in users:
    #             if user[1] == "uz":
    #                 bot.send_photo(user[0],
    #                                photo='https://api.ama.uz/media/images/products/Screenshot_2022-10-03_at_17.55.11.png',
    #                                caption=text, parse_mode='HTML')
    #
    #             else:
    #                 bot.send_photo(user[0],
    #                                photo='https://api.ama.uz/media/images/products/Screenshot_2022-10-03_at_17.55.11.png',
    #                                caption=text_ru, parse_mode='HTML')
    #     respon = {
    #         "status": 1,
    #         "msg": "Order add",
    #         "order_id": count
    #     }
    # except:
    #     print("Unexpected error:", sys.exc_info()[0])
    #     respon = {
    #         "status": 0,
    #         "msg": "Order not add",
    #         "order_id": "none"
    #     }
    # return Response(respon)


#
# #send CallBack
# class CallBack(APIView):
#     def post(self, request):
#         requests = json.load(request)
#         users = getUser()
#         try:
#             user_name = requests['fio']
#             user_phone = requests['phone']
#             callback = models.CallBack.objects.create(
#                 name=user_name,
#                 phone=user_phone
#             )
#             line = '                                                 '
#
#             text = f"<b>🙎‍♂️ F.I.O:  {user_name}</b>\n"
#             text_ru = f"<b>🙎‍♂️ F.I.O:  {user_name}</b>\n"
#
#             text += line + '\n'
#             text_ru += line + '\n'
#
#             text += f"<b>📞  Foydalanuvchi raqami:  +{user_phone}</b>\n"
#             text_ru += f"<b>📞  Номер пользователя:  +{user_phone}</b>\n"
#
#
#
#             for user in users:
#                 if user[1] == "uz":
#
#                     bot.send_photo(user[0], photo=f'https://api.apotheca.uz/media/Images/callme1.png',  caption=text, parse_mode='HTML')
#
#                 else:
#                     bot.send_photo(user[0], photo=f'https://api.apotheca.uz/media/Images/callme1.png', caption=text_ru, parse_mode='HTML')
#
#             respon = {
#                 "status": 1,
#                 "msg": "callback add",
#                 "order_id": callback.id
#             }
#         except:
#             print("Unexpected error:", sys.exc_info()[0])
#             respon = {
#                 "status": 0,
#                 "msg": "callback not add",
#                 "order_id": "none"
#             }
#         return Response(respon)
#
#
#
#
# password select


# user select
def getUser():
    chat_id = TelegramUsers.objects.filter(status=True, block_bot=True).all().values_list('chat_id', 'language')

    return chat_id


#
#
#

# number fixing
def fixing(init):
    price_str = str(init)[::-1]
    count = 1
    fix = ""
    for item in range(len(price_str)):
        if count % 3 == 0:
            fix += price_str[item] + " "

        else:
            fix += price_str[item]
        count += 1

    fix = fix[::-1]
    return fix


#
# # WebhookBOT_TOKEN = "2010700988:AAH8ZNrtHug-wiAAbCFG1s8GcI6EAvz7Mpo"
# https://api.telegram.org/bot5643766729:AAFUsCVimWgHzdW1LOaVwgnXS_8jp-z-4dM/setWebhook?url=https://351d-213-230-116-72.eu.ngrok.io/api/bot/bot_manager/

@api_view(['GET'])
def test(request):
    score = LessonPassing.objects.filter(user_id=1, lesson__course_id=3).values('lesson_id').annotate(
        Max('score')).aggregate(Sum('score__max'))
    max_score = LessonPassing.objects.filter(user_id=1, lesson__course_id=3).values('lesson_id').annotate(
        Max('max_score')).aggregate(Sum('max_score__max'))
    print(score, max_score)
    return Response({'status': 1})
